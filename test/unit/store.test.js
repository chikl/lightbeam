/* eslint no-undef: "off" */
/* eslint no-console: "off" */

/*
 * @todo resolve 'assert' is undefined and turn 'on' the above eslint rules
 * @todo use eslint-plugin-chai to resolve 'assert'
 */
'use strict';

import { store as storeImport } from '../../src/js/store.js';
import chai from '../../node_modules/chai/index.js';
var assert = chai.assert;
var expect = chai.expect;

import sinon from '../../node_modules/sinon/lib/sinon.js';

function clone(obj) {
  return Object.assign({}, obj);
}

describe('store.js', () => {
  afterEach(() => {
    // Restore the default sandbox here
    sinon.restore();
  });

  const store = storeImport;

  const mockWebsites = {
    'a1.com': {
      faviconUrl: '/link/to/url',
      firstAccess: 1234,
      lastAccess: 5678,
      thirdParties: ['a.com', 'b.com', 'c.com'],
    },
    'a2.com': {},
  };

  const mockStore = {
    _websites: null,

    init() {
      this._websites = clone(mockWebsites);
    },

    setWebsite(hostname, website) {
      const websites = clone(this._websites);
      websites[hostname] = website;
      this._write(websites);
    },

    _write(websites) {
      this._websites = clone(websites);
      return Promise.resolve();
    },

    getAll() {
      return clone(this._websites);
    },

    getFirstParty(hostname) {
      if (!hostname) {
        throw new Error('getFirstParty requires a valid hostname argument');
      }

      return this._websites[hostname];
    },

    getThirdParties(hostname) {
      if (!hostname) {
        throw new Error('getFirstParty requires a valid hostname argument');
      }

      const firstParty = this.getFirstParty(hostname);
      if ('thirdParties' in firstParty) {
        return firstParty.thirdParties;
      }

      return null;
    },

    setFirstParty(hostname, data) {
      if (!hostname) {
        throw new Error('setFirstParty requires a valid hostname argument');
      }

      this.setWebsite(hostname, data);
    },

    setThirdParty(origin, target, data) {
      if (!origin) {
        throw new Error('setThirdParty requires a valid origin argument');
      }

      const websites = clone(this._websites);
      const firstParty = clone(websites[origin]);

      if (!('thirdParties' in firstParty)) {
        firstParty['thirdParties'] = [];
      }
      firstParty['thirdParties'].push(target);

      this.setFirstParty(origin, firstParty);

      const thirdParty = websites[target] ? websites[target] : {};
      for (const key in data) {
        if (!thirdParty[key]) {
          thirdParty[key] = data[key];
        }
      }
    },
  };

  describe('store.js get method', () => {
    it('should initialise _websites to mockWebsites', (done) => {
      mockStore.init();
      const websites = mockStore.getAll();
      assert.deepEqual(websites, mockWebsites);
      done();
    });

    it('should get all websites from store', (done) => {
      const websites = mockStore.getAll();
      assert.deepEqual(websites, mockWebsites);
      done();
    });

    it('should get website object for a1.com', (done) => {
      const website = mockStore.getFirstParty('a1.com');
      assert.deepEqual(website, mockWebsites['a1.com']);
      done();
    });

    it('error thrown when hostname is not passed for getFirstParty()', () => {
      try {
        mockStore.getFirstParty();
      } catch (err) {
        console.log('error from getFirstParty', err);
      }
    });

    it('should get thirdParties for a1.com', (done) => {
      const thirdParties = mockStore.getThirdParties('a1.com');
      const mockThirdParties = mockWebsites['a1.com'].thirdParties;
      assert.deepEqual(thirdParties, mockThirdParties);
      done();
    });

    it('error thrown when hostname is not passed for getThirdParties()', () => {
      try {
        mockStore.getThirdParties();
      } catch (err) {
        console.log('error from getThirdParties', err);
      }
    });

    it('should return null for getThirdParties()', (done) => {
      const thirdParties = mockStore.getThirdParties('a2.com');
      assert.equal(thirdParties, null);
      done();
    });
  });

  describe('store.js set method', () => {
    it('should set firstParty c1.com', (done) => {
      mockStore.setFirstParty('c1.com', { faviconUrl: '/ccc/ccc' });
      const website = mockStore.getFirstParty('c1.com');
      assert.deepEqual(website, mockStore._websites['c1.com']);
      done();
    });

    it('should set thirdParty for c1.com', (done) => {
      mockStore.setThirdParty('c1.com', 'c11.com', { faviconUrl: '/c1/c1' });
      const thirdParties = mockStore.getThirdParties('c1.com');
      const mockThirdParties = mockStore._websites['c1.com'].thirdParties;
      assert.deepEqual(thirdParties, mockThirdParties);
      done();
    });
  });

  describe('store.js other methods', () => {
    it('getAllowList()', (done) => {
      const allowList = {
        Facebook: {
          properties: ['facebook.com', 'facebook.de', 'messenger.com'],
          resources: ['facebook.com', 'facebook.de', 'akamaihd.net'],
        },
        Google: {
          properties: ['abc.xyz', 'google.com', 'ingress.com'],
          resources: ['google.com', '2mdn.net'],
        },
      };
      const { firstPartyAllowList, thirdPartyAllowList } =
        store.reformatList(allowList);
      expect(firstPartyAllowList).to.be.an('object');
      expect(Object.keys(firstPartyAllowList)).to.have.lengthOf(6);
      expect(Object.keys(thirdPartyAllowList)).to.have.lengthOf(2);
      expect(thirdPartyAllowList).to.have.property('1').with.lengthOf(2);
      done();
    });

    it('outputWebsite()', (done) => {
      const hostname = 'abc.com';
      const website = {
        hostname: 'abc.com',
        faviconUrl: 'path/to/favicon',
        firstPartyHostnames: false,
        firstParty: true,
        thirdParties: [],
        isVisible: true,
        firstRequestTime: 12345,
        lastRequestTime: 98765,
      };
      const output1 = store.outputWebsite(hostname, website);
      expect(Object.keys(output1)).to.have.lengthOf(5);
      assert.isUndefined(output1.firstRequestTime);
      const output2 = store.outputWebsite(hostname, website, true);
      expect(Object.keys(output2)).to.have.lengthOf(8);
      assert.equal(output2.isVisible, website.isVisible);
      done();
    });

    it('mungeData methods', (done) => {
      const value1 = store.mungeDataInbound('isVisible', true);
      assert.equal(value1, 1);
      const value2 = store.mungeDataInbound('isVisible', false);
      assert.equal(value2, 0);
      const value3 = store.mungeDataOutbound('isVisible', 1);
      assert.equal(value3, true);
      const value4 = store.mungeDataOutbound('isVisible', 0);
      assert.equal(value4, false);
      done();
    });

    describe('setFirstParty()', () => {
      it('throw error when hostname is undefined', async () => {
        sinon.stub(store, 'setWebsite');
        let error;
        try {
          await store.setFirstParty(null, {});
        } catch (e) {
          error = e;
        } finally {
          expect(error).to.be.an('Error');
        }
      });

      it("don't throw error when hostname is defined", async () => {
        sinon.stub(store, 'setWebsite');
        let error;
        try {
          await store.setFirstParty('abc.com', {});
        } catch (e) {
          error = e;
        } finally {
          expect(error).to.be.undefined;
        }
      });
    });
  });

  describe('Test isRequestDatesTableAvailable()', () => {
    it('Table with request dates should be available', (done) => {
      store.db = {
        tables: [
          {
            name: 'requestDates',
          },
          {
            name: 'websites',
          },
        ],
      };
      const result = store.isRequestDatesTableAvailable();
      assert.equal(result, true);
      done();
    });

    it('Table with request dates should NOT be available', (done) => {
      store.db = {
        tables: [
          {
            name: 'websites',
          },
        ],
      };
      const result = store.isRequestDatesTableAvailable();
      assert.equal(result, false);
      done();
    });
  });
});

/* eslint no-undef: "off" */

'use strict';

import { lightbeam as lightbeamImport } from '../../src/js/lightbeam.js';
import chai from '../../node_modules/chai/index.js';
var assert = chai.assert;
var expect = chai.expect;

import sinon from '../../node_modules/sinon/lib/sinon.js';

const data = {
  'www.abc.com': {
    favicon: 'url',
    firstParty: true,
    firstPartyHostnames: false,
    hostname: 'www.abc.com',
    thirdParties: ['www.tracker.com', 'www.fonts.org'],
    lastRequestTime: 5,
  },
  'www.homepage.site': {
    favicon: 'url',
    firstParty: true,
    firstPartyHostnames: false,
    hostname: 'www.homepage.site',
    thirdParties: ['www.tracker.org', 'www.fonts.org'],
    lastRequestTime: 2,
  },
  'www.business.com': {
    favicon: 'url',
    firstParty: true,
    firstPartyHostnames: false,
    hostname: 'www.business.com',
    thirdParties: ['www.tracker.org', 'www.fonts.org', 'www.analytics.com'],
    lastRequestTime: 3,
  },
  'www.fonts.org': {
    favicon: 'url',
    firstParty: false,
    firstPartyHostnames: ['www.abc.com', 'www.business.com'],
    hostname: 'www.fonts.org',
    thirdParties: [],
  },
  'www.tracker.com': {
    favicon: 'url',
    firstParty: false,
    firstPartyHostnames: [
      'www.abc.com',
      'www.business.com',
      'www.homepage.site',
    ],
    hostname: 'www.tracker.com',
    thirdParties: [],
  },
  'www.analytics.com': {
    favicon: 'url',
    firstParty: false,
    firstPartyHostnames: ['www.business.com'],
    hostname: 'www.analytics.com',
    thirdParties: [],
  },
};

const lightbeam = lightbeamImport;

function changeFilters(
  maxRecentFirstPartiesActivated = false,
  minThirdPartiesActivated = false,
  maxRecentFirstParties = 10,
  minThirdParties = 0,
  timeFrame = null,
) {
  lightbeam.filters = {
    getFilter: (filterName) => {
      switch (filterName) {
        case 'maxRecentFirstPartiesActivated':
          return maxRecentFirstPartiesActivated;
        case 'minThirdPartiesActivated':
          return minThirdPartiesActivated;
        case 'maxRecentFirstParties':
          return maxRecentFirstParties;
        case 'minThirdParties':
          return minThirdParties;
        case 'timeFrame':
          return timeFrame;
      }
    },
  };
}

describe('lightbeam.js', () => {
  afterEach(() => {
    // Restore the default sandbox here
    sinon.restore();
  });

  describe('Test getDataGatheredSince()', () => {
    before(() => {
      lightbeam.store = {
        getFirstRequestTime() {
          return 1690276250000;
        },
      };
    });
    it('should give correct date strings', async () => {
      const stub = sinon.stub(lightbeam, 'getLanguage').callsFake(() => {
        return 'de';
      });
      const { dateStr, fullDateTime } = await lightbeam.getDataGatheredSince();
      assert.equal(dateStr, '25.7.2023');
      assert.equal(fullDateTime, '2023-07-25T09:10:50.000Z');
      stub.reset();
      const dateStr2 = (await lightbeam.getDataGatheredSince()).dateStr;
      assert.equal(dateStr2, 'Jul 25 2023');
    });
  });

  describe('Test transformData()', () => {
    before(() => {
      sinon.stub(lightbeam, 'createImgElementFromUrl').callsFake(() => {
        return '';
      });
    });
    it('should give correct number of nodes and links', (done) => {
      const { nodes, links } = lightbeam.transformData(data);
      expect(links).to.have.lengthOf(7);
      expect(nodes).to.have.lengthOf(6);
      done();
    });

    it('should contain specific link', (done) => {
      const { links } = lightbeam.transformData(data);
      let foundLink = false;
      for (const link of links) {
        if (
          link.source === 'www.homepage.site' &&
          link.target === 'www.tracker.org'
        ) {
          foundLink = true;
        }
      }
      assert.isTrue(foundLink);
      done();
    });
  });

  describe('Test filterData()', () => {
    beforeEach(() => {
      sinon.stub(lightbeam, 'getFirstParties').callsFake(() => {
        return new Promise((resolve) => {
          const websites = [];
          for (const hostname in data) {
            const website = data[hostname];
            if (website.firstParty) {
              websites.push(website);
            }
          }
          websites.sort((websiteA, websiteB) => {
            const a = websiteA.lastRequestTime;
            const b = websiteB.lastRequestTime;
            if (a > b) {
              return -1;
            }
            if (a < b) {
              return 1;
            }
            return 0;
          });
          resolve(websites);
        });
      });
      lightbeam.websites = Object.assign({}, data);
    });
    it('should filter out websites - maxRecentFirstParties', (done) => {
      changeFilters(true, false, 1, 3);
      lightbeam.filterData().then((result) => {
        expect(Object.keys(result)).to.have.lengthOf(3);
        done();
      });
    });

    it('should filter out websites - minThirdParties', (done) => {
      changeFilters(false, true, 2, 3);
      lightbeam.filterData().then((result) => {
        expect(Object.keys(result)).to.have.lengthOf(4);
        done();
      });
    });

    it('should filter out websites - timeFrame', (done) => {
      sinon.stub(Date, 'now').callsFake(() => {
        return 8;
      });
      sinon.stub(lightbeam, 'getLastRequest').callsFake((hostname) => {
        return data[hostname].lastRequestTime;
      });
      changeFilters(false, false, 0, 0, 1);
      lightbeam
        .filterData()
        .then((result) => {
          expect(Object.keys(result)).to.have.lengthOf(0);
        })
        .finally(() => {
          changeFilters(false, false, 0, 0, 10);
          lightbeam.filterData().then((result) => {
            expect(Object.keys(result)).to.have.lengthOf(6);
            done();
          });
        });
    });
  });

  describe('Test linkData(data)', () => {
    it('should link data correctly', (done) => {
      const uncompleteData = {
        'www.company1.com': {
          thirdParties: null,
        },
        'www.company2.com': {
          thirdParties: ['www.tracker1.com'],
        },
        'www.company3.com': {},
        'www.tracker1.com': {
          firstPartyHostnames: [
            'www.company1.com',
            'www.company2.com',
            'www.company3.com',
          ],
        },
        'www.tracker2.com': {
          firstPartyHostnames: ['www.company2.com'],
        },
      };
      lightbeam.linkData(uncompleteData);
      assert.equal(uncompleteData['www.company1.com'].thirdParties.length, 1);
      assert.equal(uncompleteData['www.company2.com'].thirdParties.length, 2);
      assert.isNotNull(uncompleteData['www.company3.com'].thirdParties);
      assert.equal(uncompleteData['www.company3.com'].thirdParties.length, 1);
      done();
    });
  });

  describe('Test fuseData(data)', () => {
    it('should fuse data correctly', (done) => {
      const fuseMaps = [
        {
          name: 'Google',
          domains: [
            '.google.',
            '.gstatic.com',
            '.doubleclick.net',
            '.recaptcha.net',
            '.googleapis.com',
            '.googletagmanager.com',
            '.googleusercontent.com',
            '.pki.goog.',
          ],
          icon: '../../images/google_icon.png',
        },
        {
          name: 'Facebook',
          domains: ['.facebook.com', '.facebook.net'],
          icon: '../../images/f_logo_RGB-Blue_512.png',
        },
        {
          name: 'Cloudflare',
          domains: ['.cloudflare.com', '.cloudflareinsights.com'],
          icon: '../../images/CF_logomark.png',
        },
      ];
      lightbeam.settings = {
        getOption: () => {
          return fuseMaps;
        },
      };
      const unfusedData = {
        'www.company1.com': {
          firstParty: true,
          hostname: 'www.company1.com',
          thirdParties: ['www.google.com', 'www.recaptcha.net'],
        },
        'www.company2.com': {
          firstParty: true,
          hostname: 'www.company2.com',
          thirdParties: [
            'www.google.com',
            'www.facebook.com',
            'www.cloudflare.com',
            'www.cloudflareinsights.com',
          ],
        },
        'www.company3.com': {
          firstParty: true,
          hostname: 'www.company3.com',
          thirdParties: [
            'www.google.com',
            'www.googletagmanager.com',
            'www.facebook.net',
          ],
        },
        'www.google.com': {
          firstParty: false,
          firstPartyHostnames: [
            'www.company1.com',
            'www.company2.com',
            'www.company3.com',
          ],
          hostname: 'www.google.com',
        },
        'www.recaptcha.net': {
          firstParty: false,
          firstPartyHostnames: ['www.company1.com'],
          hostname: 'www.recaptcha.net',
        },
        'www.googletagmanager.com': {
          firstParty: false,
          firstPartyHostnames: ['www.company3.com'],
          hostname: 'www.googletagmanager.com',
        },
        'www.facebook.com': {
          firstParty: false,
          firstPartyHostnames: ['www.company2.com'],
          hostname: 'www.facebook.com',
        },
        'www.facebook.net': {
          firstParty: false,
          firstPartyHostnames: ['www.company3.com'],
          hostname: 'www.facebook.net',
        },
        'www.cloudflare.com': {
          firstParty: false,
          firstPartyHostnames: ['www.company2.com'],
          hostname: 'www.cloudflare.com',
        },
        'www.cloudflareinsights.com': {
          firstParty: false,
          firstPartyHostnames: ['www.company2.com'],
          hostname: 'www.cloudflareinsights.com',
        },
      };
      const fusedData = lightbeam.fuseData(unfusedData);
      assert.equal(Object.keys(fusedData).length, 6);
      assert.exists(fusedData['www.company1.com']);
      assert.exists(fusedData['Google']);
      assert.notExists(fusedData['www.cloudflare.com']);
      assert.notExists(fusedData['www.google.com']);
      done();
    });
  });
});

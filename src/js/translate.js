function translate() {
  const elementsToTranslate = document.querySelectorAll('[data-i18n]');
  for (let a = 0; a < elementsToTranslate.length; a++) {
    const element = elementsToTranslate[a];
    let message = '';
    if (element.dataset && element.dataset.i18n) {
      message = browser.i18n.getMessage(element.dataset.i18n);
    }
    if (message.length > 0) {
      element.textContent = message;
    }
  }
}

window.addEventListener('load', translate);

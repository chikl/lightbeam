window.addEventListener('load', () => {
  const skipButton = document.getElementById('close-button');
  skipButton.onclick = () => {
    location.href = 'index.html';
  };
});

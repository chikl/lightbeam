'use strict';

function checkFirstRun(details) {
  if (details.reason === 'update') {
    const language = browser.i18n.getUILanguage();
    if (language === 'de') {
      browser.tabs.create({ url: 'release-notes-de.html' });
    } else {
      browser.tabs.create({ url: 'release-notes.html' });
    }
  }
}

async function runLightbeam() {
  // Checks to see if Lightbeam is already open.
  // Returns true if it is, false if not.
  async function isOpen() {
    const tabs = await browser.tabs.query({});
    const fullUrl = browser.runtime.getURL('index.html');
    const lightbeamTabs = tabs.filter((tab) => {
      return tab.url === fullUrl;
    });
    return lightbeamTabs[0] || false;
  }

  const lightbeamTab = await isOpen();
  if (!lightbeamTab) {
    // only open a new Lightbeam instance if one isn't already open.
    browser.tabs.create({ url: 'index.html' });
  } else if (!lightbeamTab.active) {
    // re-focus Lightbeam if it is already open but lost focus
    browser.tabs.update(lightbeamTab.id, { active: true });
  }
}
browser.runtime.onInstalled.addListener(checkFirstRun);

// When the user clicks browserAction icon in toolbar, run Lightbeam
browser.browserAction.onClicked.addListener(runLightbeam);

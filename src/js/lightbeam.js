import { settings } from './settings/settings.js';
import { filters } from './settings/filter.js';
import { ModalDialog } from './templates/modalDialog.js';
import { Button } from './templates/button.js';
import { Checkbox } from './templates/checkbox.js';
import { LoadingModal } from './templates/loadingModal.js';
import { store } from './store.js';

/* eslint-disable-next-line no-redeclare */
export const lightbeam = {
  db: null,
  store: null,
  websites: {},
  requestDates: [],
  dataGatheredSince: null,
  numFirstParties: 0,
  numThirdParties: 0,
  activatedGraphMode: 'graph',
  settings: null,
  filters: null,
  indexes: [
    'hostname', // Primary key
    'firstRequestTime',
    'lastRequestTime',
    'isVisible',
    'firstParty',
  ],
  showPerformanceWarning: true,
  isImporting: false,

  async init() {
    await this.initFilter();
    this.store = store;
    this.store.init();
    this.websites = await store.getAll();
    this.initDatabase();
    this.initTPToggle();
    this.initSidebarButtons();
    viz.init();
    this.redraw();
    this.addListeners();
    this.updateVars();
    this.initSettings();
  },

  clone(obj) {
    return Object.assign({}, obj);
  },

  isRequestDatesTableAvailable() {
    const tables = this.db.tables;
    return (
      tables.filter((table) => {
        return table.name === 'requestDates';
      }).length === 1
    );
  },

  createDexie() {
    if (!this.db) {
      this.db = new Dexie('websites_database');
    }
  },

  initDatabase() {
    this.createDexie();
    this.initDbTables();
    const isRequestDatesTableAvailable = this.isRequestDatesTableAvailable();
    if (isRequestDatesTableAvailable) {
      const requestDatesObservable = Dexie.liveQuery(() => {
        return this.db.requestDates.toArray();
      });
      requestDatesObservable.subscribe({
        next: (result) => {
          if (result) {
            this.requestDates = result;
            if (this.requestDates.length < result.length) {
              this.redraw();
            }
          }
        },
        error: () => {
          // TODO: Handle error
        },
      });
    }

    const websitesObservable = Dexie.liveQuery(() => {
      return this.db.websites
        .filter((website) => {
          return website.isVisible || website.firstParty;
        })
        .toArray();
    });
    websitesObservable.subscribe({
      next: (result) => {
        if (result) {
          if (Object.keys(this.websites).length < result.length) {
            const newWebsites = {};
            for (const website of result) {
              newWebsites[website.hostname] = this.store.outputWebsite(
                website.hostname,
                website,
                false,
              );
            }
            this.websites = newWebsites;
            this.redraw();
            this.updateVars();
          }
        }
      },
    });
  },

  initDbTables() {
    const websites = this.indexes.join(', ');
    this.db.version(1).stores({
      websites: websites,
    });
    this.db.version(2).stores({
      websites: websites,
      requestDates: '++, hostname, requestDate',
    });
  },

  updateDatabase() {
    return new Promise((resolve) => {
      const isRequestDatesTableAvailable = this.isRequestDatesTableAvailable();
      if (!isRequestDatesTableAvailable) {
        this.db.close();
        this.initDbTables();
        this.db.open().then(() => {
          resolve();
        });
      } else {
        resolve();
      }
    });
  },

  setTP(enableTP) {
    const enableTpButton = document.getElementById('enable-tp-button');
    const disableTpButton = document.getElementById('disable-tp-button');
    if (enableTP) {
      if (!enableTpButton.classList.contains('active')) {
        enableTpButton.classList.add('active');
      }
      if (disableTpButton.classList.contains('active')) {
        disableTpButton.classList.remove('active');
      }
      browser.privacy.websites.trackingProtectionMode.set({ value: 'always' });
    } else {
      if (enableTpButton.classList.contains('active')) {
        enableTpButton.classList.remove('active');
      }
      if (!disableTpButton.classList.contains('active')) {
        disableTpButton.classList.add('active');
      }
      browser.privacy.websites.trackingProtectionMode.set({
        value: 'private_browsing',
      });
    }
  },

  async initTPToggle() {
    const enableTpButton = document.getElementById('enable-tp-button');
    const disableTpButton = document.getElementById('disable-tp-button');
    const tpSettings = document.getElementById('tracking-protection-settings');
    if ('trackingProtectionMode' in browser.privacy.websites) {
      tpSettings.hidden = false;

      const trackingProtectionState =
        await browser.privacy.websites.trackingProtectionMode.get({});
      let value = true;
      if (trackingProtectionState.value !== 'always') {
        value = false;
      }
      this.setTP(value);
      disableTpButton.addEventListener('click', () => {
        if (
          !document
            .getElementById('disable-tp-button')
            .classList.contains('active')
        ) {
          this.setTP(false);
        }
      });
      enableTpButton.addEventListener('click', () => {
        if (
          !document
            .getElementById('enable-tp-button')
            .classList.contains('active')
        ) {
          this.setTP(true);
        }
      });
    } else {
      tpSettings.hidden = true;
    }
  },

  initSidebarButtons() {
    const createIssueButton = document.getElementById('create-issue-button');
    createIssueButton.addEventListener('click', () => {
      window.open('https://gitlab.com/chikl/lightbeam/-/issues', '_blank');
    });
    const openHomepageButton = document.getElementById('open-homepage-button');
    openHomepageButton.addEventListener('click', () => {
      const language = browser.i18n.getUILanguage();
      if (language === 'de') {
        window.open('https://www.lightbeam.chikl.de/start/', '_blank');
      } else {
        window.open('https://www.lightbeam.chikl.de/', '_blank');
      }
    });
    const writeEmailButton = document.getElementById('write-email-button');
    let addonName = browser.runtime.getManifest().name.toLowerCase();
    if (addonName.includes(' beta')) {
      addonName = addonName.replace(' beta', '');
    }
    const emailDomain = 'mail.de';
    const emailAddress = `${addonName}@${emailDomain}`;
    writeEmailButton.addEventListener('click', () => {
      window.location.href = `mailto:${emailAddress}`;
    });
    const modeButtons = document.querySelectorAll('.toggle-button.mode');
    modeButtons.forEach((button) => {
      button.addEventListener('click', () => {
        if (button.classList.contains('active')) {
          return;
        }
        modeButtons.forEach((button) => {
          button.classList.remove('active');
        });
        this.activatedGraphMode = button.id.split('-')[0];
        button.classList.add('active');
        this.redraw();
      });
    });
    const toggleSidebarButton = document.getElementById(
      'toggle-sidebar-button',
    );
    toggleSidebarButton.addEventListener('click', () => {
      const sidebar = document.getElementById('sidebar');
      const classList = sidebar.classList;
      if (classList.contains('open')) {
        classList.remove('open');
      } else {
        classList.add('open');
      }
    });
  },

  async initFilter() {
    const that = this;
    this.filters = filters;
    await filters.init(() => {
      that.redraw();
    });
  },

  initSettings() {
    const that = this;
    this.settings = settings;
    this.settings.init(() => {
      that.redraw();
    });
  },

  addListeners() {
    this.initDownloadButton();
    this.initImportButton();
    this.initResetButton();
  },

  // Called from init() (isFirstParty = undefined)
  // and redraw() (isFirstParty = true or false).
  updateVars() {
    const promises = [];
    promises.push(this.getNumFirstParties());
    promises.push(this.getNumThirdParties());
    Promise.all(promises).then(async (values) => {
      this.numFirstParties = values[0];
      this.numThirdParties = values[1];

      // initialize dynamic vars from storage
      if (!this.dataGatheredSince) {
        const { dateStr, fullDateTime } = await this.getDataGatheredSince();
        if (!dateStr) {
          return;
        }
        this.dataGatheredSince = dateStr;
        const dataGatheredSinceElement = document.getElementById(
          'data-gathered-since',
        );
        dataGatheredSinceElement.textContent = this.dataGatheredSince || '';
        dataGatheredSinceElement.setAttribute('datetime', fullDateTime || '');
      }

      this.setPartyVar();
    });
  },

  // Updates dynamic variable values in the page
  setPartyVar() {
    const numFirstPartiesElement = document.getElementById('num-first-parties');
    const numThirdPartiesElement = document.getElementById('num-third-parties');
    numFirstPartiesElement.textContent = `${this.numFirstParties}`;
    const str = `${this.numThirdParties}`;
    numThirdPartiesElement.textContent = str;
  },

  getLanguage() {
    return browser.i18n.getUILanguage();
  },

  async getDataGatheredSince() {
    const firstRequestUnixTime = await this.store.getFirstRequestTime();
    if (!firstRequestUnixTime) {
      return {};
    }
    // reformat unix time
    let fullDateTime = new Date(firstRequestUnixTime);
    let dateStr = fullDateTime.toDateString();

    // remove day of the week
    const dateArr = dateStr.split(' ');
    dateArr.shift();
    dateStr = dateArr.join(' ');

    const language = this.getLanguage();
    switch (language) {
      case 'de':
        dateStr = fullDateTime.toLocaleDateString('de-DE');
    }

    // ISO string used for datetime attribute on <time>
    fullDateTime = fullDateTime.toISOString();
    return {
      dateStr,
      fullDateTime,
    };
  },

  getNumFirstParties() {
    return this.store.getNumFirstParties();
  },

  getNumThirdParties() {
    return this.store.getNumThirdParties();
  },

  createImgElementFromUrl(url) {
    const imageElement = document.createElement('img');
    imageElement.src = url;
    return imageElement;
  },

  // transforms the object of nested objects 'websites' into a
  // usable format for d3
  /*
    websites is expected to match this format:
    {
      "www.firstpartydomain.com": {
        favicon: "http://blah...",
        firstParty: true,
        firstPartyHostnames: false,
        hostname: "www.firstpartydomain.com",
        thirdParties: [
          "www.thirdpartydomain.com",
          ...
        ]
      },
      "www.thirdpartydomain.com": {
        favicon: "",
        firstParty: false,
        firstPartyHostnames: [
          "www.firstpartydomain.com",
          ...
        ],
        hostname: "www.thirdpartydomain.com",
        thirdParties: []
      },
      ...
    }

    nodes is expected to match this format:
    [
      {
        favicon: "http://blah...",
        firstParty: true,
        firstPartyHostnames: false,
        hostname: "www.firstpartydomain.com",
        thirdParties: [
          "www.thirdpartydomain.com",
          ...
        ]
      },
      {
        favicon: "",
        firstParty: false,
        firstPartyHostnames: [
          "www.firstpartydomain.com",
          ...
        ],
        hostname: "www.thirdpartydomain.com",
        thirdParties: []
      },
      ...
    ]

    links is expected to match this format:
    [
      {
        source: {
          favicon: "http://blah...",
          firstParty: true,
          firstPartyHostnames: false,
          hostname: "www.firstpartydomain.com",
          thirdParties: [
            "www.thirdpartydomain.com",
            ...
          ]
        },
        target: {
          favicon: "",
          firstParty: false,
          firstPartyHostnames: [
            "www.firstpartydomain.com",
            ...
          ],
          hostname: "www.thirdpartydomain.com",
          thirdParties: []
        }
      },
      ...
    ]
  */
  transformData(data) {
    const nodes = [];
    let links = [];
    const websites = data ?? this.websites;
    for (const website in websites) {
      const site = JSON.parse(JSON.stringify(websites[website]));
      if (site.thirdParties) {
        const thirdPartyLinks = site.thirdParties.map((thirdParty) => {
          return {
            source: website,
            target: thirdParty,
          };
        });
        links = links.concat(thirdPartyLinks);
      }

      if (site.imageSrc) {
        site.image = this.createImgElementFromUrl(site.imageSrc);
      }

      nodes.push(site);
    }

    return {
      nodes,
      links,
    };
  },

  prepareDataForViz() {
    return new Promise((resolve) => {
      this.filterData().then((data) => {
        this.linkData(data);
        this.addRequestDatesToData(data);
        if (this.activatedGraphMode === 'fuse') {
          data = this.fuseData(data);
        }
        const transformedData = this.transformData(data);
        resolve(transformedData);
      });
    });
  },

  getFirstParties(sorted) {
    return new Promise((resolve) => {
      if (sorted) {
        this.db.websites
          .where('firstParty')
          .equals(1)
          .sortBy('lastRequestTime')
          .then((array) => {
            resolve(array.reverse());
          });
      } else {
        this.db.websites
          .where('firstParty')
          .equals(1)
          .toArray()
          .then((array) => {
            resolve(array);
          });
      }
    });
  },

  getLastRequest(hostname) {
    return new Promise((resolve) => {
      this.db.requestDates
        .where('hostname')
        .equals(hostname)
        .limit(1)
        .last()
        .then((requestDateObject) => {
          if (!requestDateObject) {
            this.db.websites.get(hostname).then((website) => {
              resolve(website.lastRequestTime);
            });
          }
          resolve(requestDateObject.requestDate);
        });
    });
  },

  filterData() {
    return new Promise((resolve) => {
      let data = null;
      if (
        this.filters.getFilter('maxRecentFirstPartiesActivated') ||
        this.filters.getFilter('minThirdPartiesActivated') ||
        this.filters.getFilter('timeFrame')
      ) {
        this.getFirstParties(
          this.filters.getFilter('maxRecentFirstPartiesActivated'),
        ).then(async (filteredWebsites) => {
          let filteredData = {};
          const max = this.filters.getFilter('maxRecentFirstPartiesActivated')
            ? this.filters.getFilter('maxRecentFirstParties')
            : null;
          let count = 1;

          for (const website of filteredWebsites) {
            if (max && count > max) {
              break;
            }
            if (
              this.filters.getFilter('timeFrame') &&
              this.filters.getFilter('timeFrame') > 0
            ) {
              const timeFrame = this.filters.getFilter('timeFrame');
              const lastRequest = await this.getLastRequest(website.hostname);
              if (Date.now() - timeFrame > lastRequest) {
                continue;
              }
            }
            if (
              !this.filters.getFilter('minThirdPartiesActivated') ||
              (this.filters.getFilter('minThirdPartiesActivated') &&
                website.thirdParties.length >=
                  this.filters.getFilter('minThirdParties'))
            ) {
              filteredData[website.hostname] = this.clone(website);
              count++;
            }
          }

          // Add third parties
          let addedFirstParty = true;
          while (addedFirstParty) {
            addedFirstParty = false;
            const filteredDataCopy = Object.assign({}, filteredData);
            for (const website in filteredData) {
              if (!filteredData[website].thirdParties) {
                filteredData[website].thirdParties = [];
              }
              for (const site in this.websites) {
                if (
                  this.websites[site].firstPartyHostnames &&
                  typeof this.websites[site].firstPartyHostnames === 'object' &&
                  this.websites[site].firstPartyHostnames?.includes(
                    filteredData[website].hostname,
                  )
                ) {
                  if (!filteredDataCopy[site]) {
                    filteredDataCopy[site] = this.clone(this.websites[site]);
                    if (this.websites[site].firstParty) {
                      addedFirstParty = true;
                    }
                  }
                }
              }
            }
            filteredData = Object.assign({}, filteredDataCopy);
          }

          // Remove first parties from firstPartyHostnames which are not in the filtered data
          for (const website in filteredData) {
            const newFirstPartyHostnames = [];
            if (filteredData[website].firstPartyHostnames) {
              for (const hostname of filteredData[website]
                .firstPartyHostnames) {
                if (filteredData[hostname]) {
                  newFirstPartyHostnames.push(hostname);
                }
              }
              filteredData[website].firstPartyHostnames =
                newFirstPartyHostnames;
            }
          }
          data = filteredData;
          resolve(data);
        });
      } else {
        data = this.websites;
        resolve(data);
      }
    });
  },

  linkData(data) {
    const websites = data ?? this.websites;
    for (const websiteHostname in websites) {
      const website = websites[websiteHostname];
      if (website.firstPartyHostnames) {
        // if we have the first parties make the link if they don't exist
        website.firstPartyHostnames.forEach((firstPartyHostname) => {
          if (data[firstPartyHostname]) {
            const firstPartyWebsite = websites[firstPartyHostname];
            if (
              !Object.hasOwn(firstPartyWebsite, 'thirdParties') ||
              firstPartyWebsite.thirdParties === null
            ) {
              firstPartyWebsite.thirdParties = [];
              firstPartyWebsite.firstParty = true;
            }
            if (!firstPartyWebsite.thirdParties.includes(websiteHostname)) {
              firstPartyWebsite.thirdParties.push(websiteHostname);
            }
          }
        });
      }
    }
  },

  addRequestDatesToData(data) {
    for (const website in data) {
      const site = data[website];
      site['requestDates'] = this.requestDates
        .filter((element) => {
          return element['hostname'] === site.hostname;
        })
        .map((element) => {
          return element.requestDate;
        });
      data[website] = site;
    }
  },

  fuseData(data) {
    const fusedWebsites = {};
    const fusedRequestDates = {};
    const handledFusedNodes = new Set();
    const websites = data ?? this.websites;
    for (const website in websites) {
      const site = JSON.parse(JSON.stringify(websites[website]));
      let partyName = site.hostname;
      if (site.thirdParties?.length > 0) {
        if (site.firstParty) {
          const handledFuses = new Set();
          let newThirdParties = site.thirdParties.filter((party) => {
            const fuseMapKey = this.nameOfFuseMap(party);
            if (fuseMapKey) {
              handledFuses.add(fuseMapKey);
              if (site.requestDates) {
                if (fusedRequestDates[fuseMapKey]) {
                  fusedRequestDates[fuseMapKey].push(...site.requestDates);
                } else {
                  fusedRequestDates[fuseMapKey] = site.requestDates;
                }
              }

              return false;
            } else {
              return true;
            }
          });
          newThirdParties = newThirdParties.concat(Array.from(handledFuses));
          site.thirdParties = newThirdParties;
        } else {
          const newHostname = this.nameOfFuseMap(site.hostname);
          partyName = newHostname;
          if (newHostname) {
            site.hostname = newHostname;
          }
        }
      }

      const mapName = this.nameOfFuseMap(website);
      if (!site.firstParty) {
        if (mapName) {
          if (!handledFusedNodes.has(mapName)) {
            const map = this.settings
              .getOption('fuseMaps')
              .filter((map) => {
                return map.name === mapName;
              })
              .at(0);
            site.imageSrc = map.icon;
            site.isFused = true;
            handledFusedNodes.add(mapName);
          }
          partyName = mapName;
          site.hostname = mapName;
        }
      }

      if (!fusedWebsites[partyName]) {
        fusedWebsites[partyName] = site;
      }
    }

    for (const entry of handledFusedNodes) {
      fusedWebsites[entry].requestDates = fusedRequestDates[entry];
    }

    return fusedWebsites;
  },

  nameOfFuseMap(domain) {
    let returnedKey;
    const fuseMaps = this.settings.getOption('fuseMaps');
    fuseMaps.forEach((map) => {
      map.domains.some((mapDomain) => {
        const result = domain.includes(mapDomain);
        if (result) returnedKey = map.name;
        return result;
      });
    });
    return returnedKey;
  },

  initDownloadButton() {
    const saveDataOptions = document.getElementById(
      'save-data-with-options-button',
    );
    const saveData = document.getElementById('save-data-button');
    const self = this;
    saveDataOptions.addEventListener('click', () => {
      const saveMenu = new ModalDialog(browser.i18n.getMessage('saveData'));
      document.body.appendChild(saveMenu.getElement());

      const checkboxFusedData = new Checkbox(
        browser.i18n.getMessage('includeFusedData'),
      );
      const saveButton = new Button('Save');
      saveButton.getElement().addEventListener('click', () => {
        self.downloadData(checkboxFusedData.isActive());
        saveMenu.getElement().remove();
      });
      saveMenu.appendChild(checkboxFusedData.getElement());
      saveMenu.appendChild(saveButton.getElement());
    });

    saveData.addEventListener('click', () => {
      self.downloadData(false);
    });
  },

  async downloadData(fused) {
    const extensionName = browser.runtime.getManifest().name;
    const version = browser.runtime.getManifest().version;
    const websites = await this.store.getAll({
      includeEverything: true,
    });
    const fusedWebsites = fused ? this.fuseData() : undefined;
    if (!this.isRequestDatesTableAvailable()) {
      await this.updateDatabase();
    }
    const requestDates = await this.db.requestDates.toArray();
    const data = {
      metaData: {
        extension: extensionName,
        lightbeamVersion: version,
      },
      websites: websites,
      fusedWebsites: fusedWebsites,
      requestDates: requestDates,
    };
    const blob = new Blob([JSON.stringify(data, ' ', 2)], {
      type: 'application/json',
    });
    const url = window.URL.createObjectURL(blob);
    const downloading = browser.downloads.download({
      url: url,
      filename: 'lightbeamData.json',
      conflictAction: 'uniquify',
    });
    await downloading;
  },

  initImportButton() {
    const importDataButton = document.getElementById('import-data-button');
    const importInput = document.getElementById('importInput');
    importDataButton.addEventListener('click', () => {
      importInput.click();
    });
    importInput.addEventListener('change', async (event) => {
      const progressDialog = new LoadingModal(
        browser.i18n.getMessage('ImportingData'),
      );
      document.body.appendChild(progressDialog.getElement());
      const files = event.target.files;
      this.isImporting = true;
      const websitesToImport = [];
      for (const file of files) {
        await file
          .text()
          .then(async (text) => {
            const json = JSON.parse(text);
            if (json.requestDates) {
              if (!this.isRequestDatesTableAvailable()) {
                await this.updateDatabase();
              }
              await this.db.requestDates.bulkPut(json.requestDates);
            }
            const websites = json.metaData ? json.websites : json;
            for (const website in websites) {
              for (const key in websites[website]) {
                const value = websites[website][key];
                // IndexedDB does not accept boolean values for indexes; using 0/1 instead
                if (value === true) {
                  websites[website][key] = 1;
                }
                if (value === false) {
                  websites[website][key] = 0;
                }
              }

              if (!websites[website].isVisible) {
                websites[website].isVisible = 1;
              }
              websitesToImport.push(websites[website]);
            }
          })
          .catch(() => {
            // TODO: Handle error
          });
      }
      await this.db.websites.bulkPut(websitesToImport);
      progressDialog.remove();
      this.updateVars();
      this.websites = await this.store.getAll();
      this.isImporting = false;
      this.redraw();
    });
  },

  initResetButton() {
    const resetDataButton = document.getElementById('reset-data-button');

    resetDataButton.addEventListener('click', () => {
      const resetDialog = new ModalDialog(
        browser.i18n.getMessage('resetData'),
        'medium',
      );
      document.body.appendChild(resetDialog.getElement());

      const text1 = document.createElement('p');
      text1.textContent = browser.i18n.getMessage('resetDialog-1');

      const text2 = document.createElement('p');
      text2.textContent = browser.i18n.getMessage('resetDialog-2');

      const cancelButton = new Button(browser.i18n.getMessage('cancel'));
      cancelButton.addEventListener('click', () => {
        resetDialog.remove();
      });

      const confirmButton = new Button(browser.i18n.getMessage('confirm'));
      confirmButton.addEventListener('click', () => {
        this.store.reset().then(() => {
          this.updateVars();
          resetDialog.remove();
          this.redraw();
        });
      });

      const buttonContainer = document.createElement('div');
      buttonContainer.classList.add('dialog-choice-buttons');

      buttonContainer.appendChild(cancelButton.getElement());
      buttonContainer.appendChild(confirmButton.getElement());

      resetDialog.appendChild(text1);
      resetDialog.appendChild(text2);
      resetDialog.appendChild(buttonContainer);
    });
  },

  showRenderingWarning(data) {
    if (!document.getElementById('warning')) {
      const vis = document.getElementById('vis');
      const warning = new ModalDialog(
        browser.i18n.getMessage('warning'),
        'medium',
        false,
        20,
      );
      warning.getElement().id = 'warning';
      const text = document.createElement('p');
      text.textContent = browser.i18n.getMessage('performanceWarning');
      const renderButton = new Button(browser.i18n.getMessage('showGraph'));
      const dontShowCheckbox = new Checkbox(
        browser.i18n.getMessage('dontShowMessageAgainInSession'),
      );
      const that = this;
      renderButton.addEventListener('click', () => {
        if (dontShowCheckbox.isActive()) {
          this.showPerformanceWarning = false;
        }
        that.continueRedraw(data);
        warning.remove();
      });

      warning.appendChild(text);
      warning.appendChild(dontShowCheckbox.getElement());
      warning.appendChild(renderButton.getElement());
      vis.appendChild(warning.getElement());
    }
  },

  redraw(data) {
    if (this.isImporting) {
      return;
    }
    if (data) {
      if (
        !(data.hostname in this.websites) ||
        (data.favicon.length > 0 &&
          this.websites[data.hostname].favicon.length === 0)
      ) {
        this.websites[data.hostname] = data;
        this.updateVars(data.firstParty);
      }
      if (data.firstPartyHostnames) {
        // if we have the first parties make the link if they don't exist
        data.firstPartyHostnames.forEach((firstPartyHostname) => {
          if (this.websites[firstPartyHostname]) {
            const firstPartyWebsite = this.websites[firstPartyHostname];
            if (!('thirdParties' in firstPartyWebsite)) {
              firstPartyWebsite.thirdParties = [];
              firstPartyWebsite.firstParty = true;
            }
            if (!firstPartyWebsite.thirdParties.includes(data.hostname)) {
              firstPartyWebsite.thirdParties.push(data.hostname);
            }
          }
        });
      }
    }
    this.prepareDataForViz().then((transformedData) => {
      if (
        this.showPerformanceWarning &&
        Object.keys(transformedData.nodes).length > 200
      ) {
        this.showRenderingWarning(transformedData);
      } else {
        this.continueRedraw(transformedData);
      }
    });
  },

  continueRedraw(transformedData) {
    viz.draw(transformedData.nodes, transformedData.links);
  },
};

window.onload = () => {
  lightbeam.init();
};

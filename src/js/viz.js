/* eslint-disable-next-line no-redeclare, no-unused-vars */
const viz = {
  scalingFactor: 2,
  circleRadius: 5,
  resizeTimer: null,
  minZoom: 0.5,
  maxZoom: 2,
  collisionRadius: 10,
  chargeStrength: -100,
  tickCount: 100,
  canvasColor: 'white',
  alphaDecay: 0.1, // Default: 0.001
  alphaStart: 1,
  alphaTargetStart: 0.1,
  alphaTargetStop: 0,

  init() {
    const { width, height } = this.getDimensions();
    const { canvas, context } = this.createCanvas();

    this.canvas = canvas;
    this.context = context;
    this.tooltip = document.getElementById('tooltip');
    this.tooltipHeader = document.getElementById('tooltip-header');
    this.tooltipContent = document.getElementById('tooltip-content');
    this.circleRadius = this.circleRadius * this.scalingFactor;
    this.collisionRadius = this.collisionRadius * this.scalingFactor;
    this.scale = (window.devicePixelRatio || 1) * this.scalingFactor;
    this.transform = d3.zoomIdentity;
    this.defaultIcon = this.loadImage('images/defaultFavicon.svg');

    this.updateCanvas(width, height);
    this.addListeners();
  },

  draw(nodes, links) {
    this.nodes = nodes;
    this.links = links;

    this.simulateForce();
    this.drawOnCanvas();
  },

  simulateForce() {
    if (!this.simulation) {
      this.simulation = d3.forceSimulation(this.nodes);
      this.simulation.alphaDecay(this.alphaDecay);
      this.simulation.on('tick', () => {
        return this.drawOnCanvas();
      });
      this.registerSimulationForces();
    } else {
      this.simulation.nodes(this.nodes);
      this.resetAlpha();
    }
    this.registerLinkForce();
  },

  resetAlpha() {
    const alpha = this.simulation.alpha();
    const alphaRounded = Math.round((1 - alpha) * 100);
    if (alphaRounded === 100) {
      this.simulation.alpha(this.alphaStart);
      this.restartSimulation();
    }
  },

  resetAlphaTarget() {
    this.simulation.alphaTarget(this.alphaTargetStart);
    this.restartSimulation();
  },

  stopAlphaTarget() {
    this.simulation.alphaTarget(this.alphaTargetStop);
  },

  restartSimulation() {
    this.simulation.restart();
  },

  registerLinkForce() {
    const linkForce = d3.forceLink(this.links);
    linkForce.id((d) => {
      return d.hostname;
    });
    this.simulation.force('link', linkForce);
  },

  registerSimulationForces() {
    const centerForce = d3.forceCenter(this.width / 2, this.height / 2);
    this.simulation.force('center', centerForce);

    const forceX = d3.forceX(this.width / 2);
    this.simulation.force('x', forceX);

    const forceY = d3.forceY(this.height / 2);
    this.simulation.force('y', forceY);

    const chargeForce = d3.forceManyBody();
    chargeForce.strength(this.chargeStrength);
    this.simulation.force('charge', chargeForce);

    const collisionForce = d3.forceCollide(this.collisionRadius);
    this.simulation.force('collide', collisionForce);
  },

  createCanvas() {
    const base = document.getElementById('visualization');
    const canvas = document.createElement('canvas');
    const context = canvas.getContext('2d', { alpha: false });

    base.appendChild(canvas);

    return {
      canvas,
      context,
    };
  },

  updateCanvas(width, height) {
    this.width = width;
    this.height = height;
    this.canvas.setAttribute('width', width * this.scale);
    this.canvas.setAttribute('height', height * this.scale);
    this.canvas.style.width = `${width}px`;
    this.canvas.style.height = `${height}px`;
    this.context.scale(this.scale, this.scale);
  },

  getDimensions() {
    const element = document.getElementById('vis');
    const { width, height } = element.getBoundingClientRect();

    return {
      width,
      height,
    };
  },

  drawOnCanvas() {
    this.context.clearRect(0, 0, this.width, this.height);
    this.context.save();
    this.context.translate(this.transform.x, this.transform.y);
    this.context.scale(this.transform.k, this.transform.k);
    this.drawLinks();
    this.drawNodes();
    this.context.restore();
  },

  getRadius(thirdPartyLength) {
    if (thirdPartyLength > 0) {
      if (thirdPartyLength > this.collisionRadius) {
        return this.circleRadius + this.collisionRadius;
      } else {
        return this.circleRadius + thirdPartyLength;
      }
    }
    return this.circleRadius;
  },

  drawNodes() {
    if (!this.nodes) {
      return;
    }
    for (const node of this.nodes) {
      const x = node.fx || node.x;
      const y = node.fy || node.y;
      let radius;

      this.context.beginPath();
      this.context.moveTo(x, y);

      if (node.firstParty) {
        radius = this.getRadius(node.thirdParties.length);
        this.drawFirstParty(x, y, radius);
      } else if (node.isFused) {
        this.drawFused(x, y);
      } else {
        this.drawThirdParty(x, y);
      }

      if (node.shadow) {
        this.drawShadow(x, y, radius);
      }

      this.context.fillStyle = this.canvasColor;
      this.context.closePath();
      this.context.fill();

      if (node.favicon) {
        this.drawFavicon(node, x, y, radius);
      } else {
        this.drawFavicon(node, x, y, this.circleRadius);
      }
    }
  },

  getSquare(radius) {
    const side = Math.sqrt(radius * radius * 2);
    const offset = side * 0.5;

    return {
      side,
      offset,
    };
  },

  loadImage(URI) {
    return new Promise((resolve, reject) => {
      if (!URI) {
        return reject();
      }

      const image = new Image();

      image.onload = () => {
        return resolve(image);
      };
      image.onerror = () => {
        return resolve(this.defaultIcon);
      };
      image.src = URI;
    });
  },

  scaleFavicon(image, side) {
    const canvas = document.createElement('canvas'),
      context = canvas.getContext('2d');

    canvas.width = side * this.scale * 0.9 * this.transform.k;
    canvas.height = side * this.scale * 0.9 * this.transform.k;
    context.fillStyle = this.canvasColor;
    context.fillRect(0, 0, canvas.width, canvas.height);

    context.drawImage(image, 0, 0, canvas.width, canvas.height);

    return context.getImageData(0, 0, canvas.width, canvas.height);
  },

  drawFavicon(node, x, y, radius) {
    const offset = node.isFused ? radius * 1.4 : this.getSquare(radius).offset,
      tx = this.transform.applyX(x - offset * 0.9),
      ty = this.transform.applyY(y - offset * 0.9);
    var side = this.getSquare(radius).side;

    if (!node.image) {
      const favicon = node.favicon ? node.favicon : node.faviconUrl;
      this.loadImage(favicon)
        .then((result) => {
          if (result) node.image = result;
        })
        .catch(() => {
          node.image = undefined;
        });
    }
    if (node.image) {
      if (node.isFused) {
        side = side * 2;
      }
      this.context.putImageData(
        this.scaleFavicon(node.image, side),
        tx * this.scale,
        ty * this.scale,
      );
    }
  },

  drawShadow(x, y, radius) {
    const lineWidth = 2,
      shadowBlur = 15,
      shadowRadius = 5;
    this.context.beginPath();
    this.context.lineWidth = lineWidth;
    this.context.shadowColor = this.canvasColor;
    this.context.strokeStyle = 'rgba(0, 0, 0, 1)';
    this.context.shadowBlur = shadowBlur;
    this.context.shadowOffsetX = 0;
    this.context.shadowOffsetY = 0;
    this.context.arc(x, y, radius + shadowRadius, 0, 2 * Math.PI);
    this.context.stroke();
    this.context.closePath();
  },

  drawFirstParty(x, y, radius) {
    this.context.arc(x, y, radius, 0, 2 * Math.PI);
  },

  drawThirdParty(x, y) {
    const deltaY = this.circleRadius / 2;
    const deltaX = deltaY * Math.sqrt(3);

    this.context.moveTo(x - deltaX, y + deltaY);
    this.context.lineTo(x, y - this.circleRadius);
    this.context.lineTo(x + deltaX, y + deltaY);
  },

  drawFused(x, y) {
    const radius = this.circleRadius * 1.4;
    this.context.moveTo(x - radius, y + radius);
    this.context.lineTo(x + radius, y + radius); // to right bottom
    this.context.lineTo(x + radius, y - radius); // to right top
    this.context.lineTo(x - radius, y - radius); // to left top
    this.context.lineTo(x - radius, y + radius); // to left bottom
  },

  getTooltipPosition(x, y) {
    const tooltipArrowHeight = 20;
    const { right: canvasRight } = this.canvas.getBoundingClientRect();
    const { height: tooltipHeight, width: tooltipWidth } =
      this.tooltip.getBoundingClientRect();
    const top = y - tooltipHeight - this.circleRadius - tooltipArrowHeight;

    let left;
    if (x + tooltipWidth >= canvasRight) {
      left = x - tooltipWidth;
    } else {
      left = x - tooltipWidth / 2;
    }

    return {
      left,
      top,
    };
  },

  showTooltip(title, content, x, y) {
    this.tooltipHeader.innerText = title;
    this.tooltipContent.innerText = content;
    this.tooltip.style.display = 'block';

    const { left, top } = this.getTooltipPosition(x, y);
    this.tooltip.style['left'] = `${left}px`;
    this.tooltip.style['top'] = `${top}px`;
  },

  hideTooltip() {
    this.tooltip.style.display = 'none';
  },

  drawLinks() {
    if (this.links) {
      this.context.beginPath();
      for (const d of this.links) {
        const sx = d.source.fx || d.source.x;
        const sy = d.source.fy || d.source.y;
        const tx = d.target.fx || d.target.x;
        const ty = d.target.fy || d.target.y;
        this.context.moveTo(sx, sy);
        this.context.lineTo(tx, ty);
      }
      this.context.closePath();
    }
    this.context.strokeStyle = '#ccc';
    this.context.stroke();
  },

  isPointInsideCircle(x, y, node) {
    const dx = Math.abs(x - node.x);
    const dy = Math.abs(y - node.y);
    const d = dx * dx + dy * dy;
    const r = node.isFused ? this.circleRadius * 1.4 : this.circleRadius;

    return d <= r * r;
  },

  getNodeAtCoordinates(x, y) {
    if (this.nodes) {
      for (const node of this.nodes) {
        if (this.isPointInsideCircle(x, y, node)) {
          return node;
        }
      }
    }
    return null;
  },

  getMousePosition(event) {
    const { left, top } = this.canvas.getBoundingClientRect();

    return {
      mouseX: event.clientX - left,
      mouseY: event.clientY - top,
    };
  },

  addListeners() {
    this.addMouseMove();
    this.addWindowResize();
    this.addDrag();
    this.addZoom();
  },

  addMouseMove() {
    this.canvas.addEventListener('mousemove', (event) => {
      const { mouseX, mouseY } = this.getMousePosition(event);
      const [invertX, invertY] = this.transform.invert([mouseX, mouseY]);
      const node = this.getNodeAtCoordinates(invertX, invertY);

      if (node) {
        const numberOfCalls = node.requestDates?.length || 0;
        const requestsString =
          browser.i18n.getMessage('requests').at(0).toUpperCase() +
          browser.i18n.getMessage('requests').substring(1);
        this.showTooltip(
          node.hostname,
          `${requestsString}: ${numberOfCalls}`,
          mouseX,
          mouseY,
        );
      } else {
        this.hideTooltip();
      }
    });
  },

  addWindowResize() {
    window.addEventListener('resize', () => {
      clearTimeout(this.resizeTimer);
      this.resizeTimer = setTimeout(() => {
        this.resize();
      }, 250);
    });
  },

  resize() {
    this.canvas.style.width = 0;
    this.canvas.style.height = 0;

    const { width, height } = this.getDimensions('visualization');
    this.updateCanvas(width, height);
    if (this.nodes && this.links) {
      this.draw(this.nodes, this.links);
    }
  },

  addDrag() {
    const drag = d3.drag();
    drag.subject((event) => {
      return this.dragSubject(event);
    });
    drag.on('start', (event) => {
      return this.dragStart(event);
    });
    drag.on('drag', (event) => {
      return this.drag(event);
    });
    drag.on('end', (event) => {
      return this.dragEnd(event);
    });

    d3.select(this.canvas).call(drag);
  },

  dragSubject(event) {
    const x = this.transform.invertX(event.x);
    const y = this.transform.invertY(event.y);
    return this.getNodeAtCoordinates(x, y);
  },

  dragStart(event) {
    if (!event.active) {
      this.resetAlphaTarget();
    }
    event.subject.shadow = true;
    event.subject.fx = event.subject.x;
    event.subject.fy = event.subject.y;
  },

  drag(event) {
    event.subject.fx = event.x;
    event.subject.fy = event.y;

    this.hideTooltip();
  },

  dragEnd(event) {
    if (!event.active) {
      this.stopAlphaTarget();
    }
    event.subject.x = event.subject.fx;
    event.subject.y = event.subject.fy;
    event.subject.fx = null;
    event.subject.fy = null;
    event.subject.shadow = false;
  },

  addZoom() {
    const zoom = d3.zoom().scaleExtent([this.minZoom, this.maxZoom]);
    zoom.on('zoom', (event) => {
      return this.zoom(event);
    });

    d3.select(this.canvas).call(zoom);
  },

  zoom(event) {
    this.transform = event.transform;
    this.drawOnCanvas();
  },
};

export class Checkbox {
  constructor(text) {
    this.checkbox = document.createElement('div');
    this.checkbox.classList.add('checkbox');
    this.checkbox.addEventListener('click', () => {
      this.toggle();
    });

    const box = document.createElement('div');
    box.classList.add('checkbox-box');

    const checkmarkIcon = document.createElement('img');
    checkmarkIcon.src = '../../icons/ic_fluent_checkmark_24_filled_white.svg';
    box.appendChild(checkmarkIcon);

    const label = document.createElement('span');
    label.textContent = text;
    label.classList.add('checkbox-label');

    this.checkbox.appendChild(box);
    this.checkbox.appendChild(label);
  }

  getElement() {
    return this.checkbox;
  }

  isActive() {
    return this.checkbox.classList.contains('active');
  }

  toggle() {
    if (this.checkbox.classList.contains('active')) {
      this.checkbox.classList.remove('active');
    } else {
      this.checkbox.classList.add('active');
    }
  }
}

export class Modal {
  constructor(isClosable, zindex = 25) {
    this.isClosable = isClosable;

    this.modal = document.createElement('div');
    this.modal.classList.add('modal-dialog');
    this.modal.style.zIndex = `${zindex}`;
    this.modal.addEventListener('click', () => {
      if (this.isClosable) {
        this.modal.remove();
      }
    });

    this.container = document.createElement('div');
    this.container.classList.add('modal-dialog-container');
    this.container.addEventListener('click', (event) => {
      event.stopPropagation();
    });

    this.modal.appendChild(this.container);
  }

  getElement() {
    return this.modal;
  }

  remove() {
    this.modal.remove();
  }
}

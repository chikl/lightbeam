export class Button {
  constructor(text, src) {
    this.button = document.createElement('div');
    this.button.classList.add('button');

    if (src) {
      const icon = document.createElement('img');
      icon.src = src;
      label.classList.add('button-image');
      this.button.appendChild(icon);
    }

    const label = document.createElement('span');
    label.textContent = text;
    label.classList.add('button-text');

    this.button.appendChild(label);
  }

  getElement() {
    return this.button;
  }

  addEventListener(event, method) {
    this.button.addEventListener(event, method);
  }
}

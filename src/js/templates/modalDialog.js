import { Modal } from './modal.js';

export class ModalDialog extends Modal {
  /**
   * @param {String} title Title for the dialog
   * @param {String} size small, medium, or large
   * @param {Boolean} isClosable Should users be able to close this dialog?
   * @param {Number} zindex The CSS zindex of the modal
   */
  constructor(title, size = 'small', isClosable = true, zindex = 25) {
    super(isClosable, zindex);
    const modalDialogHeader = document.createElement('div');
    modalDialogHeader.classList.add('modal-dialog-header');

    const modalDialogHeaderText = document.createElement('h3');
    modalDialogHeaderText.textContent = title;
    modalDialogHeaderText.classList.add('modal-dialog-header-text');

    this.content = document.createElement('div');
    this.content.classList.add('modal-dialog-content');
    if (size === 'medium') {
      this.content.classList.add('medium');
    } else if (size === 'large') {
      this.content.classList.add('large');
    }

    modalDialogHeader.appendChild(modalDialogHeaderText);

    if (isClosable) {
      const modalDialogCloseButton = document.createElement('div');
      modalDialogCloseButton.classList.add('modal-dialog-close-button');
      modalDialogCloseButton.addEventListener('click', () => {
        this.modal.remove();
      });

      const closeIcon = document.createElement('img');
      closeIcon.src = '../../icons/ic_fluent_dismiss_24_regular_white.svg';

      modalDialogCloseButton.appendChild(closeIcon);

      modalDialogHeader.appendChild(modalDialogCloseButton);
    }

    this.buttonContainer = document.createElement('div');
    this.buttonContainer.classList.add('modal-dialog-choice-buttons');

    this.container.appendChild(modalDialogHeader);
    this.container.appendChild(this.content);
    this.container.appendChild(this.buttonContainer);
  }

  appendChild(element) {
    this.content.appendChild(element);
  }

  addButton(button) {
    this.buttonContainer.appendChild(button);
  }
}

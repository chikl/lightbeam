import { Modal } from './modal.js';

export class LoadingModal extends Modal {
  constructor(title) {
    super(false);

    const span = document.createElement('span');
    span.classList.add('animation-title');
    span.textContent = title;

    const loadingAnimation = document.createElement('img');
    loadingAnimation.src = '../../animations/lightbeam_animation.svg';
    loadingAnimation.classList.add('animation', 'loading-animation');

    this.container.appendChild(span);
    this.container.appendChild(loadingAnimation);
  }
}

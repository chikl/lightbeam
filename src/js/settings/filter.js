import '../../ext-libs/dexie.js';

export const filters = {
  defaultFilterSettings: {
    minThirdPartiesActivated: false,
    minThirdParties: 0,
    maxRecentFirstPartiesActivated: true,
    maxRecentFirstParties: 100,
    timeFrame: 0,
  },
  filterSettings: {},
  db: null,
  indexes: [
    'name', // Primary key
    'settings',
  ],

  async init(applyCallback) {
    this.applyCallback = applyCallback;
    this.initDatabase();
    await this.initValues();

    const filterToggle = document.getElementById('filter-toggle');
    filterToggle.addEventListener('click', () => {
      const filter = document.getElementById('filter');
      if (filter.classList.contains('active')) {
        filter.classList.remove('active');
      } else {
        filter.classList.add('active');
      }
    });

    // Minimum of Third Parties
    const minThirdPartiesActivator = document.getElementById(
      'min-third-parties-activator',
    );
    const minThirdPartiesInput = document.getElementById('min-third-parties');
    minThirdPartiesActivator.addEventListener('click', () => {
      if (minThirdPartiesActivator.classList.contains('active')) {
        minThirdPartiesActivator.classList.remove('active');
        minThirdPartiesInput.disabled = true;
      } else {
        minThirdPartiesActivator.classList.add('active');
        minThirdPartiesInput.disabled = false;
      }
    });

    minThirdPartiesInput.addEventListener('input', (event) => {
      const minThirdPartiesDisplay = document.getElementById(
        'min-third-parties-display',
      );
      minThirdPartiesDisplay.textContent = event.target.value;
      this.filterSettings['minThirdParties'] = event.target.value;
    });

    // Maximum of recent First Parties
    const maxRecentFirstPartiesActivator = document.getElementById(
      'max-recent-first-parties-activator',
    );
    const maxRecentFirstPartiesInput = document.getElementById(
      'max-recent-first-parties',
    );
    maxRecentFirstPartiesActivator.addEventListener('click', () => {
      if (maxRecentFirstPartiesActivator.classList.contains('active')) {
        maxRecentFirstPartiesActivator.classList.remove('active');
        maxRecentFirstPartiesInput.disabled = true;
      } else {
        maxRecentFirstPartiesActivator.classList.add('active');
        maxRecentFirstPartiesInput.disabled = false;
      }
    });

    const applyFiltersButton = document.getElementById('apply-filters-button');
    applyFiltersButton.addEventListener('click', () => {
      this.applyFilters();
    });
  },

  initDatabase() {
    this.db = new Dexie('settings_database');
    const settings = this.indexes.join(', ');
    this.db.version(1).stores({ settings });
  },

  async initValues() {
    // Time frame
    const timeFrame = (await this.db.settings.get('timeFrame'))?.settings;
    if (timeFrame) {
      const timeFrameButtons = document.getElementsByName(
        'time-frame-selection',
      );
      for (const button of timeFrameButtons) {
        if (button.value === timeFrame) {
          button.checked = true;
        }
      }
      this.filterSettings.timeFrame = timeFrame;
    } else {
      this.filterSettings.timeFrame = this.defaultFilterSettings.timeFrame;
    }

    // Minimum of Third Parties
    const minThirdPartiesActivator = document.getElementById(
      'min-third-parties-activator',
    );
    const minThirdPartiesInput = document.getElementById('min-third-parties');
    const minThirdPartiesActivated = (
      await this.db.settings.get('minThirdPartiesActivated')
    )?.settings;
    if (minThirdPartiesActivated) {
      minThirdPartiesInput.disabled = minThirdPartiesActivated === 0;
      if (minThirdPartiesActivated === 1) {
        minThirdPartiesActivator.classList.add('active');
      }
    } else {
      this.filterSettings.minThirdPartiesActivated =
        this.defaultFilterSettings.minThirdPartiesActivated;
      minThirdPartiesInput.disabled =
        this.defaultFilterSettings.minThirdPartiesActivated === false;
      if (this.defaultFilterSettings.minThirdPartiesActivated) {
        minThirdPartiesActivator.classList.add('active');
      }
    }

    const minThirdPartiesDisplay = document.getElementById(
      'min-third-parties-display',
    );
    const minThirdParties = (await this.db.settings.get('minThirdParties'))
      ?.settings;
    if (minThirdParties) {
      minThirdPartiesDisplay.textContent = minThirdParties;
      minThirdPartiesInput.value = minThirdParties;
    } else {
      this.filterSettings.minThirdParties =
        this.defaultFilterSettings.minThirdParties;
      minThirdPartiesDisplay.textContent =
        this.defaultFilterSettings.minThirdParties;
      minThirdPartiesInput.value = this.defaultFilterSettings.minThirdParties;
    }

    // Maximum of recent First Parties
    const maxRecentFirstPartiesActivator = document.getElementById(
      'max-recent-first-parties-activator',
    );
    const maxRecentFirstPartiesInput = document.getElementById(
      'max-recent-first-parties',
    );
    const maxRecentFirstPartiesActivated = (
      await this.db.settings.get('maxRecentFirstPartiesActivated')
    )?.settings;
    if (maxRecentFirstPartiesActivated) {
      maxRecentFirstPartiesInput.disabled =
        maxRecentFirstPartiesActivated === 0;
      if (maxRecentFirstPartiesActivated === 1) {
        maxRecentFirstPartiesActivator.classList.add('active');
      }
    } else {
      this.filterSettings.maxRecentFirstPartiesActivated =
        this.defaultFilterSettings.maxRecentFirstPartiesActivated;
      maxRecentFirstPartiesInput.disabled =
        this.defaultFilterSettings.maxRecentFirstPartiesActivated === false;
      if (this.defaultFilterSettings.maxRecentFirstPartiesActivated) {
        maxRecentFirstPartiesActivator.classList.add('active');
      }
    }

    const maxRecentFirstParties = (
      await this.db.settings.get('maxRecentFirstParties')
    )?.settings;
    if (maxRecentFirstParties) {
      maxRecentFirstPartiesInput.value = maxRecentFirstParties;
    } else {
      this.filterSettings.maxRecentFirstParties =
        this.defaultFilterSettings.maxRecentFirstParties;
      maxRecentFirstPartiesInput.value =
        this.defaultFilterSettings.maxRecentFirstParties;
    }

    maxRecentFirstPartiesInput.addEventListener('input', (event) => {
      const regex = /\d/i;
      if (event.data && !event.data.match(regex)) {
        maxRecentFirstPartiesInput.value =
          maxRecentFirstPartiesInput.value.slice(0, -1);
      }
    });

    const changeInputButtons = document.querySelectorAll(
      '[data-number-of-nodes]',
    );
    for (const button of changeInputButtons) {
      button.addEventListener('click', () => {
        maxRecentFirstPartiesInput.value =
          Number(maxRecentFirstPartiesInput.value) +
          Number(button.dataset.numberOfNodes);
        if (maxRecentFirstPartiesInput.value < 0) {
          maxRecentFirstPartiesInput.value = 0;
        }
      });
    }
  },

  getFilter(nameOfFilter) {
    return this.filterSettings[nameOfFilter];
  },

  applyFilters() {
    // Time frame
    const timeFrameButtons = document.getElementsByName('time-frame-selection');
    for (const radioButton of timeFrameButtons) {
      if (radioButton.checked) {
        this.db.settings.put({
          name: 'timeFrame',
          settings: radioButton.value,
        });
        this.filterSettings.timeFrame = radioButton.value;
      }
    }

    const minThirdPartiesActivator = document.getElementById(
      'min-third-parties-activator',
    );
    const isMinThirdPartiesActivatorActive =
      minThirdPartiesActivator.classList.contains('active');
    this.filterSettings.minThirdPartiesActivated =
      isMinThirdPartiesActivatorActive;
    this.db.settings.put({
      name: 'minThirdPartiesActivated',
      settings: isMinThirdPartiesActivatorActive ? 1 : 0,
    });

    const minThirdPartiesInput = document.getElementById('min-third-parties');
    this.filterSettings.minThirdParties = minThirdPartiesInput.value;
    this.db.settings.put({
      name: 'minThirdParties',
      settings: minThirdPartiesInput.value,
    });

    const maxRecentFirstPartiesActivator = document.getElementById(
      'max-recent-first-parties-activator',
    );
    const isMaxRecentFirstPartiesActivatorActive =
      maxRecentFirstPartiesActivator.classList.contains('active');
    this.filterSettings.maxRecentFirstPartiesActivated =
      isMaxRecentFirstPartiesActivatorActive;
    this.db.settings.put({
      name: 'maxRecentFirstPartiesActivated',
      settings: isMaxRecentFirstPartiesActivatorActive ? 1 : 0,
    });

    const maxRecentFirstPartiesInput = document.getElementById(
      'max-recent-first-parties',
    );
    this.filterSettings.maxRecentFirstParties =
      maxRecentFirstPartiesInput.value;
    this.db.settings.put({
      name: 'maxRecentFirstParties',
      settings: maxRecentFirstPartiesInput.value,
    });

    this.applyCallback();
  },
};

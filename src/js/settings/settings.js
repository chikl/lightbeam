import '../../ext-libs/dexie.js';
import { fuseMaps as initialFuseMaps } from './fuseMaps.js';
import { ModalDialog } from '../templates/modalDialog.js';
import { Button } from '../templates/button.js';

export const settings = {
  settings: { fuseMaps: null },
  idCount: 1,
  settingsDb: null,
  indexes: [
    'name', // Primary key
    'settings',
  ],
  errorClass: 'light-red-border',
  dialog: null,

  async init(applyCallback) {
    this.applyCallback = applyCallback;
    this.initSettingsDatabase();
    const fuseMapsObject = await this.settingsDb.settings.get('fuseMaps');
    if (fuseMapsObject) {
      this.settings.fuseMaps = fuseMapsObject.settings;
    } else {
      this.settings.fuseMaps = initialFuseMaps;
    }
    this.initSettingsButton();
  },

  initSettingsDatabase() {
    this.settingsDb = new Dexie('settings_database');
    const settings = this.indexes.join(', ');
    this.settingsDb.version(1).stores({ settings });
    this.settingsDb.open();
  },

  initSettingsButton() {
    const openSettingsButton = document.getElementById('open-settings-button');
    openSettingsButton.addEventListener('click', () => {
      this.dialog = new ModalDialog(
        browser.i18n.getMessage('settings'),
        'large',
      );
      document.body.appendChild(this.dialog.getElement());
      const fuseMapsElement = document.createElement('div');
      fuseMapsElement.id = 'fuse-maps';

      const cancelButton = new Button(browser.i18n.getMessage('cancel'));
      cancelButton.addEventListener('click', () => {
        this.dialog.remove();
      });

      const confirmButton = new Button(browser.i18n.getMessage('confirm'));
      confirmButton.addEventListener('click', () => {
        const isSuccessful = this.saveNewMaps();
        if (isSuccessful) this.dialog.remove();
      });

      this.dialog.addButton(cancelButton.getElement());
      this.dialog.addButton(confirmButton.getElement());

      this.dialog.appendChild(fuseMapsElement);
      this.initFuseMapsSettings();
    });
  },

  initFuseMapsSettings() {
    const allFuseMaps = document.getElementById('fuse-maps');
    allFuseMaps.replaceChildren();
    let count = 1;
    this.idCount = 1;
    this.settings.fuseMaps.forEach((map) => {
      const fuseMapContainer = this.createFuseMapContainer(map, count);
      this.idCount++;
      allFuseMaps.appendChild(fuseMapContainer);
      count++;
    });
    const addButton = document.createElement('div');
    addButton.classList.add('add-button');
    addButton.classList.add('positive');
    const addIcon = document.createElement('img');
    addIcon.src = 'icons/ic_fluent_add_24_filled.svg';
    addButton.appendChild(addIcon);
    addButton.addEventListener('click', () => {
      const fuseMapContainer = this.createFuseMapContainer();
      allFuseMaps.appendChild(fuseMapContainer);
      allFuseMaps.removeChild(addButton);
      allFuseMaps.appendChild(addButton);
    });
    allFuseMaps.appendChild(addButton);
  },

  /**
   * @param {String} option
   * @returns {*}
   */
  getOption(option) {
    return this.settings[option];
  },

  createFuseMapContainer(map, count) {
    if (!count) count = this.idCount++;
    const fuseMapContainer = document.createElement('div');
    fuseMapContainer.classList.add('fusemap-container', 'column');
    fuseMapContainer.id = `fuse-map-container-${count}`;

    const iconButton = this.createIconButton(count, map?.icon);

    const fuseMapName = document.createElement('input');
    fuseMapName.type = 'text';
    fuseMapName.value =
      map?.name ?? browser.i18n.getMessage('customGroupPlaceholder') + count;
    fuseMapName.id = `fuse-map-name-${count}`;
    fuseMapName.classList.add('fuse-group-name');

    const fuseMapMembers = document.createElement('textarea');
    fuseMapMembers.id = `fuse-map-members-${count}`;
    fuseMapMembers.rows = 8;
    if (map && map.domains) {
      fuseMapMembers.textContent = map.domains ? map.domains.join('\n') : '';
    }

    /* Element to show error messages */
    const errorMessageElement = document.createElement('label');
    errorMessageElement.id = `fuse-group-error-${count}`;
    errorMessageElement.classList.add('error-message');

    /* Button to delete group */
    const deleteButton = document.createElement('div');
    deleteButton.classList.add('button');
    const deleteButtonIcon = document.createElement('img');
    deleteButtonIcon.src = 'icons/ic_fluent_subtract_24_filled_white.svg';
    const deleteButtonText = document.createElement('span');
    deleteButtonText.textContent = browser.i18n.getMessage('deleteFuseGroup');
    deleteButton.appendChild(deleteButtonIcon);
    deleteButton.appendChild(deleteButtonText);
    deleteButton.addEventListener('click', () => {
      fuseMapContainer.remove();
    });

    fuseMapContainer.appendChild(iconButton);
    fuseMapContainer.appendChild(fuseMapName);
    fuseMapContainer.appendChild(fuseMapMembers);
    fuseMapContainer.appendChild(deleteButton);
    fuseMapContainer.appendChild(errorMessageElement);

    fuseMapName.addEventListener('input', () => {
      if (!this.isTextFieldEmpty(fuseMapName)) {
        fuseMapName.classList.remove(this.errorClass);
        errorMessageElement.value = '';
      }
    });

    fuseMapMembers.addEventListener('input', () => {
      if (!this.isTextAreaEmpty(fuseMapMembers)) {
        fuseMapMembers.classList.remove(this.errorClass);
        errorMessageElement.value = '';
      }
    });

    return fuseMapContainer;
  },

  createIconButton(index, icon) {
    const rootElement = document.createElement('div');
    rootElement.classList.add('icon-container');

    // Image element for 'real' icons
    const fuseGroupIcon = document.createElement('img');
    fuseGroupIcon.id = `fuse-group-icon-${index}`;
    fuseGroupIcon.classList.add('fuse-group-icon');

    // Hidden input to allow to upload custom images
    const fuseIconInput = document.createElement('input');
    const id = `fuse-icon-input-${index}`;
    fuseIconInput.id = id;
    fuseIconInput.classList.add('visually-hidden');
    fuseIconInput.type = 'file';
    fuseIconInput.accept = 'image/*';

    // Clickable label for the hidden input
    const fuseIconLabel = document.createElement('label');
    fuseIconLabel.htmlFor = id;

    // Placeholder
    const iconPlaceholder = document.createElement('img');
    iconPlaceholder.id = `fuse-icon-placeholder-${index}`;
    iconPlaceholder.classList.add('fuse-group-icon-placeholder');
    iconPlaceholder.src = 'icons/ic_fluent_image_24_regular.svg';

    fuseIconLabel.appendChild(fuseGroupIcon);
    fuseIconLabel.appendChild(iconPlaceholder);

    if (!icon) {
      fuseGroupIcon.classList.add('invisible');
    } else {
      iconPlaceholder.classList.add('invisible');
      fuseGroupIcon.src = icon;
    }
    rootElement.appendChild(fuseIconInput);
    rootElement.appendChild(fuseIconLabel);

    fuseIconInput.addEventListener(
      'change',
      (event) => {
        this.handleNewIcon(event);
      },
      false,
    );

    return rootElement;
  },

  getIndexOfElement(element) {
    return element.id.at(-1);
  },

  validateFuseMap(container, nameElement, domainsElement, imageElement) {
    const index = this.getIndexOfElement(container);
    const imgSrc = imageElement.src;
    const errorMessageElement = document.getElementById(
      `fuse-group-error-${index}`,
    );
    const errorClass = this.errorClass;
    const isNameEmpty = this.isTextFieldEmpty(nameElement);
    const isDomainListEmpty = this.isTextAreaEmpty(domainsElement);
    if (isNameEmpty && isDomainListEmpty && imgSrc?.length === 0) {
      container.remove();
      return false;
    } else if (isNameEmpty && isDomainListEmpty) {
      nameElement.classList.add(errorClass);
      domainsElement.classList.add(errorClass);
      errorMessageElement.textContent =
        "Fuse groups aren't allowed to have empty names or member lists.";
      return false;
    } else if (isNameEmpty) {
      nameElement.classList.add(errorClass);
      errorMessageElement.textContent =
        'Please give enter a name for the group.';
      return false;
    } else if (isDomainListEmpty) {
      domainsElement.classList.add(errorClass);
      errorMessageElement.textContent =
        'Please enter at least one rule for this group.';
      return false;
    }
    return true;
  },

  isTextFieldEmpty(element) {
    return element.value.length === 0;
  },

  isTextAreaEmpty(element) {
    return element.value.trim().length === 0;
  },

  isUniqueName(targetElement, className) {
    const allElementsOfGroup = document.getElementsByClassName(className);
    const duplicates = [];
    for (const element of allElementsOfGroup) {
      if (
        element.id !== targetElement.id &&
        element.value === targetElement.value
      ) {
        duplicates.push(element);
      }
    }
    if (duplicates.length !== 0) {
      const errorMessage = 'Please use different names for the fuse groups.';
      targetElement.classList.add(this.errorClass);
      const indexOfTargetElement = this.getIndexOfElement(targetElement);
      const errorMessageElementForTarget = document.getElementById(
        `fuse-group-error-${indexOfTargetElement}`,
      );
      errorMessageElementForTarget.textContent = errorMessage;
      duplicates.forEach((dup) => {
        dup.classList.add(this.errorClass);
        const index = this.getIndexOfElement(dup);
        const errorMessageElement = document.getElementById(
          `fuse-group-error-${index}`,
        );
        errorMessageElement.textContent = errorMessage;
      });
      return false;
    }
    return true;
  },

  resetAllErrorMessages() {
    const errorMessageElements =
      document.getElementsByClassName('error-message');
    for (const element of errorMessageElements) {
      element.textContent = '';
    }
  },

  handleNewIcon(event) {
    const index = this.getIndexOfElement(event.target);
    const imgElementId = `fuse-group-icon-${index}`;
    const imgElement = document.getElementById(imgElementId);
    imgElement.classList.remove('invisible');
    const imgUrl = window.URL.createObjectURL(event.target.files[0]);
    imgElement.src = imgUrl;
    document
      .getElementById(`fuse-icon-placeholder-${index}`)
      .classList.add('invisible');
  },

  saveNewMaps() {
    this.resetAllErrorMessages();
    const newFuseMaps = [];
    const fuseMapContainer = document.getElementById('fuse-maps').children;
    for (const container of fuseMapContainer) {
      if (container.classList.contains('fusemap-container')) {
        const index = this.getIndexOfElement(container);
        const nameElement = document.getElementById(`fuse-map-name-${index}`);
        const name = nameElement.value;
        const domainsElement = document.getElementById(
          `fuse-map-members-${index}`,
        );
        const domains = domainsElement.value;
        const listOfDomains = domains.split('\n');
        const imageElement = document.getElementById(
          `fuse-group-icon-${index}`,
        );
        const imgSrc = imageElement.src;
        const isValid = this.validateFuseMap(
          container,
          nameElement,
          domainsElement,
          imageElement,
        );
        const isUniqueName = this.isUniqueName(nameElement, 'fuse-group-name');
        if (!isValid || !isUniqueName) return false;
        newFuseMaps.push({
          name: name,
          domains: listOfDomains,
          icon: imgSrc,
        });
      }
    }
    this.updateFuseMaps(newFuseMaps);
    return true;
  },

  updateFuseMaps(fuseMaps) {
    this.settings.fuseMaps = fuseMaps;
    this.settingsDb.settings.put({
      name: 'fuseMaps',
      settings: fuseMaps,
    });
    this.applyCallback();
  },
};

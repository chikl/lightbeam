export const fuseMaps = [
  {
    name: 'Google',
    domains: [
      '.google.',
      '.gstatic.com',
      '.doubleclick.net',
      '.recaptcha.net',
      '.googleapis.com',
      '.googletagmanager.com',
      '.googleusercontent.com',
      '.pki.goog.',
    ],
    icon: '../../images/google_icon.png',
  },
  {
    name: 'Facebook',
    domains: ['.facebook.com', '.facebook.net'],
    icon: '../../images/f_logo_RGB-Blue_512.png',
  },
  {
    name: 'Cloudflare',
    domains: ['.cloudflare.com', '.cloudflareinsights.com'],
    icon: '../../images/CF_logomark.png',
  },
  {
    name: 'Amazon',
    domains: ['.amazonaws.com', '.cloudfront.net', '.amazontrust.com'],
    icon: '../../images/Amazon_logo_square.svg',
  },
  {
    name: 'Microsoft',
    domains: [
      '.microsoft.com',
      '.azure.com',
      '.live.com',
      '.azure.net',
      '.windows.net',
    ],
    icon: '../../images/Microsoft_logo.png',
  },
];

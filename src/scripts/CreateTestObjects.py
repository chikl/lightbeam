#! /bin/python

import argparse
import json
import time


def addWebsite(websites: object, hostname: str, lastRequestTime: int):
  websites[hostname] = {
    "hostname": hostname,
    "firstParty": True,
    "thirdParties": [],
    "lastRequestTime": lastRequestTime
  }

if __name__ == "__main__":
  parser = argparse.ArgumentParser()

  parser.add_argument(
    '-d',
    metavar = 'COUNT',
    nargs = '?',
    type = int,
    default = 10,
    help = 'How many website objects to create that are less than one day old.',
    dest = 'day'
  )

  parser.add_argument(
    '-w',
    metavar = 'COUNT',
    nargs = '?',
    type = int,
    default = 10,
    help = 'How many website objects to create that are more than one day but less than one day old.',
    dest = 'week'
  )

  parser.add_argument(
    '-m',
    metavar = 'COUNT',
    nargs = '?',
    type = int,
    default = 10,
    help = 'How many website objects to create that are more than one week and less than one month old.',
    dest = 'month'
  )

  parser.add_argument(
    '-y',
    metavar = 'COUNT',
    nargs = '?',
    type = int,
    default = 10,
    help = 'How many website objects to create that are more than one month and less than one year old.',
    dest = 'year'
  )

  args = parser.parse_args()
  day = args.day
  week = args.week
  month = args.month
  year = args.year

  websites = {}
  count = 0

  now = round(time.time() * 1000)

  day_ago = now - 86400000 + 600000
  for d in range(day):
    addWebsite(websites, "website" + str(count), now)
    count = count + 1
  
  week_ago = now - 604800000 + 600000
  for w in range(week):
    addWebsite(websites, "website" + str(count), week_ago)
    count = count + 1

  month_ago = now - 2592000000 + 600000
  for m in range(month):
    addWebsite(websites, "website" + str(count), month_ago)
    count = count + 1

  year_ago = now - 31536000000 + 600000
  for m in range(year):
    addWebsite(websites, "website" + str(count), year_ago)
    count = count + 1

  with open("test.json", "w") as file:
    json.dump(websites, file, indent=2)



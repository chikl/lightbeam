# Attention: Lightbeam moved to Codeberg

Lightbeam has a new [home on Codeberg](https://codeberg.org/chikl/Lightbeam). Read more about it on https://lightbeam.chikl.de/.

# Firefox Lightbeam

This is an add-on for visualizing HTTP requests between websites in real time, originally developed by Mozilla.

Mozilla stopped the development in 2019 (https://support.mozilla.org/en-US/kb/lightbeam-extension-firefox-no-longer-supported) so the source code (https://github.com/mozilla/lightbeam-we) of that project was used to create this fork.

![lightbeam-screenshot](/docs/images/lightbeam.gif)

## Quick Start

### Clone the repository

**Note** This repository uses a [submodule](https://github.com/mozilla-services/shavar-prod-lists) to allow some third party requests. To ensure the submodule is cloned along with this repository, use a modified `clone` command:
`git clone --recursive https://gitlab.com/chikl/lightbeam.git`

### Run the web extension

There are a couple ways to try out this web extension:

1. Open Firefox and load `about:debugging` in the URL bar.

   - Click the on "This Firefox" on the left side and on the button ["Load Temporary Add-on"](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Temporary_Installation_in_Firefox) and select the `manifest.json` file within the directory of this repository.
   - You should now see the Lightbeam icon on the top right bar of the browser.
   - Click the Lightbeam icon to launch the web extension.

2. Install the [web-ext](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Getting_started_with_web-ext) tool, change into the `src` directory of this repository, and type `web-ext run`.
   - This will launch Firefox and install the extension automatically.
   - This tool gives you some additional development features such as [automatic reloading](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Getting_started_with_web-ext#Automatic_extension_reloading).

## Development Guide

### Prepare your development environment

It might be necessary to update Nodejs and/or npm. An easy way is to add the appropriate repository to your distribution and then update
Nodejs: https://github.com/nodesource/distributions/blob/master/README.md.

### Download dependencies and build the extension

Run `npm run build`.

### Update the submodule

To manually update the submodule at any time during development, run `git submodule update`.

### Testing

Run `npm run test` to check that everything is OK.

- If you have installed `eslint` globally, you will have to install globally the following `eslint` plugins too:
  - `eslint-plugin-json`
  - `eslint-plugin-mocha`
- Test suites include lint and unit testing. You can individually run lint or unit tests using the following commands:
  - `npm run lint:eslint`
  - `npm run test:karma`

Eslint is used for linting. Karma, Mocha & Chai are used for unit testing. Additionally the test suites are run on the Travis service providing continuous integration support.

## Assets

- Icons: Microsoft Fluent Icons (https://github.com/microsoft/fluentui-system-icons)
- Google Logo: https://about.google/brand-resource-center/logos-list/
- Facebook Logo: https://about.meta.com/brand/resources/facebookapp/logo
- Cloudflare Logo: https://www.cloudflare.com/logo/
- Amazon Logo: https://upload.wikimedia.org/wikipedia/commons/a/a9/Amazon_logo.svg (edited)
- Microsoft Logo: https://upload.wikimedia.org/wikipedia/commons/4/44/Microsoft_logo.svg

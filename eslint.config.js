import js from '@eslint/js';
import globals from 'globals';
import mocha from 'eslint-plugin-mocha';
import unsanitized from 'eslint-plugin-no-unsanitized';
import prettier from 'eslint-config-prettier';

export default [
  js.configs.recommended,
  {
    ignores: [
      'src/_locales/*',
      'shavar-prod-lists/*',
      'src/ext-libs/*',
      'docs/*',
      '*.*',
    ],
  },
  {
    files: ['**/*.js'],
    languageOptions: {
      ecmaVersion: 2022,
      globals: {
        browser: 'readonly',
        store: 'readonly',
        capture: 'readonly',
        lightbeam: 'readonly',
        viz: 'readonly',
        d3: 'readonly',
        Dexie: 'readonly',
        document: 'readonly',
        window: 'readonly',
        ...globals.browser,
      },
      sourceType: 'module',
    },
    plugins: {
      mocha,
      'no-unsanitized': unsanitized,
    },
    rules: {
      'no-console': ['error'],
      'no-unsanitized/method': 'error',
      'no-unsanitized/property': 'error',
      'space-infix-ops': ['error'],
      indent: [
        'error',
        2,
        {
          CallExpression: { arguments: 1 },
          FunctionDeclaration: { parameters: 1 },
          SwitchCase: 1,
        },
      ],
      'linebreak-style': ['error', 'unix'],
      'object-property-newline': ['error'],
      'no-multi-assign': ['error'],
      'no-new-object': ['error'],
      'func-call-spacing': ['error', 'never'],
      'brace-style': ['error', '1tbs'],
      'object-curly-newline': ['error', { multiline: true }],
      'no-implied-eval': ['error'],
      'operator-linebreak': ['error', 'before'],
      'no-lonely-if': ['error'],
      'no-multi-str': ['error'],
      'prefer-const': ['error'],
      'prefer-template': ['error'],
      'require-await': ['error'],
      'spaced-comment': ['error', 'always'],
      'max-len': [
        'error',
        {
          code: 80,
          tabWidth: 2,
          ignoreUrls: true,
        },
      ],
      semi: ['error', 'always'],
      quotes: ['error', 'single'],
      'no-trailing-spaces': ['error'],
      'keyword-spacing': [
        'error',
        {
          before: true,
          after: true,
        },
      ],
      'space-before-blocks': [
        'error',
        {
          functions: 'always',
          keywords: 'always',
        },
      ],
      'arrow-body-style': ['error', 'always'],
    },
  },
  prettier,
];
